"""Cloudmod sources module
Sources are dicts that map OHLC column names to specific data vendors, e.g.
coinmarketcap_TMC, binance, poloniex, bittrex
"""

# Used for Coinmaketcap Total Market Capitalization
COINMARKETCAP_TMC = dict(
    index='date',
    open='open',
    high='high',
    low='low',
    close='close',
    volume='volume'
)

SOURCES = {'coinmarketcap_TMC': COINMARKETCAP_TMC}
