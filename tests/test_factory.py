from cloudmod import factory
import pytest


def test_get_theme():
    assert factory.get_theme('light')['colors']['increasing'] == '#00CC00'


def test_get_theme_exception():
    with pytest.raises(Exception, match="Theme not found ''"):
        factory.get_theme('') == True

    with pytest.raises(Exception, match="Theme not found 'cool'"):
        factory.get_theme('cool') == True


def test_get_themes():
    themes = factory.get_themes()
    assert themes != None
    assert 'light' in themes


def test_get_skeleton():
    skeletons = factory.get_skeleton()
    assert skeletons != None
    assert skeletons['base_additions']['xaxis']['side'] == 'bottom'


def test_get_source():
    sources = factory.get_source('coinmarketcap_TMC')
    assert sources != None
    assert sources['index'] == 'date'


def tele_get_source_exception():
    with pytest.raises(Exception, match="Source not found ''"):
        factory.get_source('')
    with pytest.raises(Exception, match="Source not found 'fake_source'"):
        factory.get_source('fake_source')


def test_get_sources():
    sources = factory.get_sources()
    assert sources != None
    assert 'coinmarketcap_TMC' in sources


def test_make_colors():
    one = {'increasing': 'color1', 'decreasing': 'color2'}
    two = {'grey': 'color2', 'grey_light': 'color8', 'decreasing': 'color3'}

    assert factory.make_colors(one, two) == {
        'increasing': 'color1', 'decreasing': 'color3', 'grey': 'color2', 'grey_light': 'color8'}


def test_make_colors_exception():
    with pytest.raises(Exception, match='Invalid keyword'):
        factory.make_colors('increasing', 'test')
    with pytest.raises(Exception, match='Invalid keyword'):
        factory.make_colors('test', 'primary')
