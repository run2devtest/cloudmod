from cloudmod import chart
import datetime as dt
import pytest
import pandas as pd


date = [
    '2018-03-28',
    '2018-03-29',
    '2018-03-30',
    '2018-03-31',
    '2018-04-01']

open = [
    263489000000,
    268063000000,
    254848000000,
    278462000000,
    300350000000]

high = [
    263489000000,
    268063000000,
    254848000000,
    278462000000,
    300350000000]

low = [
    263489000000,
    268063000000,
    254848000000,
    278462000000,
    300350000000]

close = [
    263489000000,
    268063000000,
    254848000000,
    278462000000,
    300350000000]

test_ticker_data = {'date': date, 'open': open, 'high': high, 'low': low, 'close': close}


def test_chart_init_string_options():
    df = pd.DataFrame(test_ticker_data)
    test_chart = chart.Chart(df, src='coinmarketcap_TMC', ticker='TMC',
                             start='2017-03-30', end='2017-04-30')


def test_chart_init_type_options():
    df = pd.DataFrame(test_ticker_data)
    test_chart = chart.Chart(df, src=None, ticker=False,
                             start=False, end=False)


def test_chart_init_datetime_options():
    df = pd.DataFrame(test_ticker_data)
    _start = dt.datetime(1985, 12, 31)
    _end = dt.datetime(1986, 12, 31)

    test_chart = chart.Chart(df, src=None, ticker=None,
                             start=_start, end=_end)


def test_chart_init_ind():
    df = pd.DataFrame(test_ticker_data)

    test_chart = chart.Chart(df, src=None, ticker=None,
                             start=None, end=None)
    assert list(test_chart.ind.index) == [0, 1, 2, 3, 4]


@pytest.fixture(scope='module')
def chart_instance():
    df = pd.DataFrame(test_ticker_data)
    test_chart = chart.Chart(df, src='coinmarketcap_TMC', ticker='TMC',
                             start=None, end=None)

    yield test_chart


def test_chart_to_repr(chart_instance):
    assert repr(chart_instance).split('\n')[
        0] == '          close        date          high           low          open'
    assert repr(chart_instance).split('\n')[
        1] == '0  263489000000  2018-03-28  263489000000  263489000000  263489000000'
    assert repr(chart_instance).split('\n')[
        2] == '1  268063000000  2018-03-29  268063000000  268063000000  268063000000'
    assert repr(chart_instance).split('\n')[
        3] == '2  254848000000  2018-03-30  254848000000  254848000000  254848000000'
    assert repr(chart_instance).split('\n')[
        4] == '3  278462000000  2018-03-31  278462000000  278462000000  278462000000'
    assert repr(chart_instance).split('\n')[
        5] == '4  300350000000  2018-04-01  300350000000  300350000000  300350000000'


def test_chart_len(chart_instance):
    assert len(chart_instance) == 5


def test_chart_shape(chart_instance):
    assert chart_instance.shape == (5, 5)


def test_to_frame(chart_instance):
    pass  # TODO: Test for indicators


def test_has_open(chart_instance):
    assert chart_instance.has_open == True


def test_has_high(chart_instance):
    assert chart_instance.has_high == True


def test_has_low(chart_instance):
    assert chart_instance.has_low == True


def test_has_close(chart_instance):
    assert chart_instance.has_close == True


def test_has_volume(chart_instance):
    assert chart_instance.has_volume == False


def test_has_ohlc(chart_instance):
    assert chart_instance.has_OHLC == True


def test_has_ohlcv(chart_instance):
    assert chart_instance.has_OHLCV == False


def test_to_figure_valid_kwargs_exception(chart_instance):
    with pytest.raises(Exception, match="Invalid keyword 'bad'."):
        chart_instance.to_figure(bad='fake')


# def test_to_figure_kind_to_type(chart_instance):
    # chart_instance.to_figure(kind='ohlc')
    # assert chart_instance.to_figure.type == 'ohlc'
