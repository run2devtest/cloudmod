from cloudmod import datetools
import datetime as dt
import pytest


def test_get_date_from_today():
    pass


def test_string_to_date():
    string_date = '19851231'
    datetime_date = dt.date(1985, 12, 31)

    assert datetools.string_to_date(string_date) == datetime_date


def test_string_to_int():
    int_date = 19851231
    datetime_date = dt.date(1985, 12, 31)

    assert datetools.int_to_date(int_date) == datetime_date


def test_date_to_int():
    datetime_date = dt.date(1985, 12, 31)
    int_date = 19851231

    assert datetools.date_to_int(datetime_date) == int_date
