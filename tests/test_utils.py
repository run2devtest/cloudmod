from cloudmod import utils
import pytest


def test_update():
    dict1 = {'one': {'a': '1'}}
    dict2 = {'two': {'c': '2'}}
    target = {'one': {'a': '1'}, 'two': {'c': '2'}}

    assert utils.update(dict1, dict2) == target


def test_deep_update():
    # need to add something to test deep vs shallow copys
    dict1 = {'one': {'a': '1'}}
    dict2 = {'two': {'c': '2'}}
    target = {'one': {'a': '1'}, 'two': {'c': '2'}}

    assert utils.deep_update(dict1, dict2) == target


def test_kwargs_check():
    VALID_KWARGS = {'kwarg_one', 'kwarg_two'}
    utils.kwargs_check({'kwarg_one'}, VALID_KWARGS)
    utils.kwargs_check({'kwarg_two'}, VALID_KWARGS)


def test_kwargs_check_error():
    VALID_KWARGS = {'kwarg_one', 'kwarg_two'}
    with pytest.raises(Exception, match='Invalid keyword kwarg_three.'):
        utils.kwargs_check({'kwarg_three', }, VALID_KWARGS)


def test_load_json_dict():
    pass


def test_save_json_dict():
    pass


def test_save_json_dict_error():
    with pytest.raises(Exception, match="Couldn't save because 'json_dict' was not a dictionary."):
        utils.save_json_dict('test', list())
