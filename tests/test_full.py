from requests_html import HTMLSession
import pandas as pd
import matplotlib.pyplot as plt
import cloudmod as cm


url = 'https://graphs2.coinmarketcap.com/global/marketcap-total/'
url = 'https://graphs2.coinmarketcap.com/global/marketcap-total/1491054420000/1522590420000/'
url = 'https://graphs2.coinmarketcap.com/global/marketcap-total/1514818020000/1522590420000/'
# url = 'https://graphs2.coinmarketcap.com/currencies/blockcat/1514821753000/1522594153000/'
session = HTMLSession()
r = session.get(url)
json_response = r.json()
market_cap = pd.DataFrame(json_response['market_cap_by_available_supply'])
volume_usd = pd.DataFrame(json_response['volume_usd'])

market_cap.columns = ['date', 'market_cap']
volume_usd.columns = ['date', 'volume_usd']

data = market_cap.merge(volume_usd, on='date')
data['date'] = pd.to_datetime(data['date'], unit='ms')

data.set_index('date', inplace=True)

data.market_cap = data.market_cap.astype(int)
data.volume_usd = data.volume_usd.astype(int)


data_ohlc = data['market_cap'].resample('4H').ohlc()
data_volume = data['volume_usd'].resample('4H').ohlc()

# print(data_volume.tail())
print(data_ohlc.columns)

# data.plot()
# plt.show()
ch = cm.Chart(data_ohlc)

print(ch.has_open)
print(ch.has_high)
print(ch.has_low)
print(ch.has_close)

print(ch.has_OHLC)
print(ch.has_OHLCV)

# ch.plot(kind='candlestick', volume=False, title='Total Market Capitalization')
# ch.plot(kind='candlestick', volume=False, title='Blockcat')
